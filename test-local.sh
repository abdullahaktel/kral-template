#! /usr/bin/env bash

# Exit in case of error
set -e

if [ $(uname -s) = "Linux" ]; then
    echo "Remove __pycache__ files"
    sudo find . -type d -name __pycache__ -exec rm -r {} \+
fi


# Check .env for aws credentials
if test -f ".env"; then
    source .env
fi

# If aws credentials not provided try to use pip global.index-url
if [ -z "${AWS_SECRET_ACCESS_KEY}" ]; then
    echo "AWS_SECRET_ACCESS_KEY not found in .env file. Using pip global index url"
    PIP_INDEX_URL=$(pip config get global.index-url 2> /dev/null) || : ${PIP_INDEX_URL:="http://"}

    # Check if we can reach to codeartifact
    status_code=$(curl --write-out '%{http_code}' --head --silent --output /dev/null "${PIP_INDEX_URL}algo-utils/") || : ${status_code:=400}
    if [ ${status_code} -ne "200" ] ; then
        printf "\033[0;31mAWS credentials not set. Enter credentials to .env or use aws codeartifact login \n" >&2
        exit 1
    fi
fi


# Install pre-commit
if which pre-commit >& /dev/null;
    then pip3 install -qU pre-commit;
fi;
if [ -d .git -a ! -x .git/hooks/pre-commit -a -e .pre-commit-config.yaml ];
    then pre-commit install;
fi;

# Run pre-commit
if [ -e .pre-commit-config.yaml ];
    then pre-commit run --all-files;
fi;
